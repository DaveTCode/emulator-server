db_sql = [
  """CREATE TABLE system (
        id INTEGER PRIMARY KEY NOT NULL UNIQUE,
        displayName TEXT NOT NULL UNIQUE,
        emulatorLink TEXT,
        gameRankingsSystemName TEXT)""",
  """CREATE TABLE games (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
        name TEXT NOT NULL,
        genre TEXT NOT NULL,
        systemId INTEGER NOT NULL,
        image BLOB,
        link TEXT,
        rating INTEGER,
        FOREIGN KEY(systemId) REFERENCES system(id))""",
  """CREATE TABLE systemWikiUrls (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
        systemId INTEGER NOT NULL,
        url TEXT NOT NULL UNIQUE,
        FOREIGN KEY(systemId) REFERENCES system(id))""",

  # Pre configured systems
  """INSERT INTO system (id, displayName, emulatorLink, gameRankingsSystemName)
     VALUES (1, 'Super Nintendo Enterntainment System', NULL, 'snes')""",
  """INSERT INTO system (id, displayName, emulatorLink, gameRankingsSystemName)
     VALUES (2, 'Nintendo Enterntainment System', NULL, 'nes')""",
  """INSERT INTO system (id, displayName, emulatorLink, gameRankingsSystemName)
     VALUES (3, 'Sega Genesis', NULL, 'genesis')""",
  """INSERT INTO system (id, displayName, emulatorLink, gameRankingsSystemName)
     VALUES (4, 'Game Boy Advance', NULL, 'gba')""",
  """INSERT INTO system (id, displayName, emulatorLink, gameRankingsSystemName)
     VALUES (5, 'Nintendo 64', NULL, 'n64')""",
  """INSERT INTO system (id, displayName, emulatorLink, gameRankingsSystemName)
     VALUES (6, 'MS-DOS', NULL, 'pc')""",

  # Pre configured wiki urls for game lists.
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (1, 'http://en.wikipedia.org/wiki/List_of_Super_Nintendo_Entertainment_System_games')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (1, 'http://en.wikipedia.org/wiki/List_of_Super_Famicom_games_(A-H)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (1, 'http://en.wikipedia.org/wiki/List_of_Super_Famicom_games_(I-R)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (1, 'http://en.wikipedia.org/wiki/List_of_Super_Famicom_games_(S-Z)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (2, 'http://en.wikipedia.org/wiki/List_of_Nintendo_Entertainment_System_games')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (3, 'http://en.wikipedia.org/wiki/List_of_Sega_Genesis_games')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (4, 'http://en.wikipedia.org/wiki/List_of_Game_Boy_Advance_games')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (5, 'http://en.wikipedia.org/wiki/List_of_Nintendo_64_games')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(0-9)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(A)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(B)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(C)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(D)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(E)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(F)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(G)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(H)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(I)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(J)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(K)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(L)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(M)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(N)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(O)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(P)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(Q)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(R)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(S)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(T)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(U)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(V)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(W)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(X)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(Y)')""",
  """INSERT INTO systemWikiUrls (systemId, url)
     VALUES (6, 'http://en.wikipedia.org/wiki/Index_of_MS-DOS_games_(Z)')"""
]
