import argparse
import base64
import configparser
from flask import Flask, request, jsonify
import os
import shutil
import sqlite3
import subprocess
import tempfile
import time

import importer

SECONDS_IN_DAY = 24 * 60 * 60

app = Flask(__name__, static_url_path='/static')
config = None

@app.route("/", methods=['GET'])
def index():
    return app.send_static_file('index.html')

@app.route("/js/<path:path>", methods=['GET'])
def get_js(path):
    return app.send_static_file("js/" + path)

@app.route("/css/<path:path>", methods=['GET'])
def get_css(path):
    return app.send_static_file("css/" + path)

@app.route("/fonts/<path:path>", methods=['GET'])
def get_fonts(path):
    return app.send_static_file("fonts/" + path)

@app.route("/favicon.ico", methods=['GET'])
def get_favicon():
    return app.send_static_file("img/favicon.ico")

@app.route("/partials/<path:path>", methods=['GET'])
def get_partial(path):
    return app.send_static_file("partials/" + path)

@app.route("/gimg/<id>", methods=['GET'])
def get_game_image(id):
    path = "static/game_images/" + id

    if not os.path.isfile(path) or time.time() - os.path.getmtime(path) > SECONDS_IN_DAY:
        conn = sqlite3.connect(config['library']['db_location'])
        try:
            cur = conn.cursor()
            cur.execute("SELECT image FROM games WHERE id = ?", (id,))
            image_bytes = cur.fetchone()[0]

            if image_bytes is None:
                shutil.copyfile("static/img/stencil_controller.png", path)
            else:
                with open(path, "wb") as f:
                    f.write(image_bytes)
        finally:
            conn.close()

    return app.send_static_file("game_images/" + id)

@app.route("/settings", methods=['GET'])
def get_settings():
    settings = {
        "library": config['library']['library_location'],
        "database": config['library']['db_location'],
        "emulators": []
    }

    conn = sqlite3.connect(config['library']['db_location'])
    try:
        em_cur = conn.cursor()

        for row in em_cur.execute("SELECT id, displayName, emulatorLink, gameRankingsSystemName FROM system"):
            wk_cur = conn.cursor()
            urls = []
            for wk_row in wk_cur.execute("SELECT id, url FROM systemWikiUrls WHERE systemId=?", (row[0],)):
                urls.append({
                    "id": wk_row[0],
                    "url": wk_row[1]
                })

            settings["emulators"].append({
                "id": row[0],
                "displayName": row[1],
                "path": row[2],
                "gameRankingsName": row[3],
                "urls": urls
            })
    finally:
        conn.close()

    return jsonify(settings)

@app.route("/settings", methods=['POST'])
def save_settings():
    library_location = request.json['library']
    db_location = request.json['database']

    if library_location:
        config.set('library', 'library_location', library_location)
    if db_location:
        config.set('library', 'db_location', db_location)

    with open('app.config', 'w') as configfile:
        config.write(configfile)

    conn = sqlite3.connect(config['library']['db_location'])
    try:
        for emulator in request.json['emulators']:
            cur = conn.cursor()

            if emulator["id"] != -1:
                if emulator["isDeleted"]:
                    cur.execute("DELETE FROM systemWikiUrls WHERE systemId=?", (emulator["id"],))
                    cur.execute("DELETE FROM games WHERE systemId=?", (emulator["id"],))
                    cur.execute("DELETE FROM system WHERE id=?", (emulator["id"],))
                else:
                    cur.execute("UPDATE system SET displayName=?, emulatorLink=?, gameRankingsSystemName=? WHERE id=?",
                                (emulator["displayName"], emulator["path"], emulator["gameRankingsName"], emulator["id"]))
            else:
                if not emulator["isDeleted"]:
                    cur.execute("INSERT INTO system (id, displayName, emulatorLink, gameRankingsSystemName) SELECT MAX(id) + 1,?,?,? FROM system",
                                (emulator["displayName"], emulator["path"], emulator["gameRankingsName"]))

                    emulator["id"] = cur.lastrowid

            for url in emulator["urls"]:
                if url["id"] != -1:
                    if url["isDeleted"]:
                        cur.execute("DELETE FROM systemWikiUrls WHERE id=?", (url["id"],))
                    else:
                        cur.execute("UPDATE systemWikiUrls SET url=? WHERE id=?",
                                    (url["url"], url["id"]))
                else:
                    if not url["isDeleted"]:
                        cur.execute("INSERT INTO systemWikiUrls (systemId, url) VALUES (?,?)",
                                    (emulator["id"], url["url"]))
    finally:
        conn.commit()
        conn.close()

    return "", 200

@app.route("/systems", methods=['GET'])
def get_systems():
    conn = sqlite3.connect(config['library']['db_location'])
    try:
        cur = conn.cursor()
        result = {"result":[]}
        for row in cur.execute("SELECT system.id, system.displayName FROM system ORDER BY system.displayName"):
            result["result"].append({
                "id": row[0],
                "displayName": row[1]
            })
    finally:
        conn.close()

    return jsonify(result)

@app.route("/games", methods=['GET'])
def get_games():
    conn = sqlite3.connect(config['library']['db_location'])
    try:
        cur = conn.cursor()
        result = {"result":[]}
        for row in cur.execute("SELECT games.id, games.name, games.genre, system.displayName, games.link, games.rating FROM games INNER JOIN system ON system.id = games.systemId"):
            result["result"].append({"id": row[0], "name": row[1], "genre": row[2], "system": row[3], "link": row[4], "rating": row[5] })
    finally:
        conn.close()

    return jsonify(result)

@app.route("/games/<id>", methods=['DELETE'])
def delete_game(id):
    conn = sqlite3.connect(config['library']['db_location'])
    try:
        cur = conn.cursor()
        cur.execute("DELETE FROM games WHERE games.id = ?", (id,))
        conn.commit()

        shutil.rmtree(os.path.join(config['library']['library_location'], str(id)), ignore_errors=True)

        image_file = os.path.join('static', 'game_images', str(id))
        if os.path.exists(image_file):
            os.remove(image_file)
    finally:
        conn.close()

    return "", 200

@app.route("/games/<id>/<disc_id>", methods=['POST'])
def play_game(id, disc_id):
    conn = sqlite3.connect(config['library']['db_location'])
    try:
        cur = conn.cursor()
        cur.execute("SELECT system.emulatorLink FROM games INNER JOIN system ON system.id = games.systemId WHERE games.id = ?", (id,))
        emulator_link = cur.fetchone()[0]
        file_location = os.path.join(config['library']['library_location'], str(id), str(disc_id))

        if emulator_link:
            subprocess.Popen([emulator_link, file_location])
        else:
            return "No emulator set", 200
    finally:
        conn.close()

    return "", 200

@app.route("/games/find", methods=['POST'])
def find_game():
    try:
        i = importer.RomImporter(config)
        game_link, actual_name = i.find_file_on_wiki(request.json['system']['id'], request.json['filename'])
        if game_link:
            return jsonify({'url': game_link, 'name': actual_name})
        else:
            return "Importer failed to find information on this game", 501
    except Exception as e:
        return str(e), 400

@app.route("/games", methods=['POST'])
def import_game():
    try:
        i = importer.RomImporter(config)
        result = i.import_file(request.json['url'],
                               request.json['name'],
                               base64.b64decode(request.json['fileString'].replace('data:;base64,','').encode('utf-8')),
                               request.json['system']['id'],
                               request.json['disc'])
        return result, 200
    except Exception as e:
        print(e)
        return str(e), 400

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', required=False, help='Add this flag to run the web server in debug mode', action="store_true")
    parser.add_argument('-f', '--configfile', required=False, help='Specify the location of the application config file', default='app.config')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read(args.configfile)

    # Ensure that the application structure is set up correctly.
    importer.first_time_setup(config)

    if args.debug:
        app.debug = True

    app.run(host="0.0.0.0")
