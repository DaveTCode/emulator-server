import argparse
from bs4 import BeautifulSoup
import configparser
import db_create
import os
import re
import shutil
import sqlite3
import urllib.parse
import urllib.request

class RomImporter():
    system_url_map = {}

    def __init__(self, config):
        self.config = config

    def import_directory(self, dir, regex, system_type_id):
        '''
            Import an entire directory worth of ROMs recursing into sub directories.
        '''
        for root, dirs, files in os.walk(dir):
            for f in [f for f in files if re.match(regex, f)]:
                game_link, actual_name = self.find_file_on_wiki(system_type_id, f)

                if game_link:
                    print("Found {0} at {1}".format(actual_name, game_link))
                    try:
                        with open(os.path.join(root, f), "rb") as rom_file:
                            self.import_file(game_link, actual_name, rom_file.read(), system_type_id, 1)
                    except Exception as e:
                        # Don't bail on exceptions that happen for a single ROM.
                        print(str(e))
                else:
                    print("Failed to find {0}".format(f))

    def _find_possible_game_names(self, filename):
        '''
            Lots of ROMs have filenames indicating the region (using (J) for example) and other
            marking from whichever system they were retrieved.

            This function converts those into a list of the possible game names which we can
            search for on the various rom databases.
        '''
        # First strip out the file extension as no sane database uses that form
        if filename.find(".") != -1:
            filename = filename[:filename.rfind(".")]

        # Add the name so far as the only possible option.
        possible_game_names = [filename]

        # Strip out any instances of (U) and [!] etc
        removed_brackets = re.sub(r'[\(\[].*?[\)\]]', '', filename).strip()
        if not removed_brackets in possible_game_names:
            possible_game_names.append(removed_brackets)

        # For any result so far it's possible that the database will have removed the ", the" from
        # the filename.
        to_add = []
        for n in possible_game_names:
            if n.lower().find(", the") != -1:
                no_the = re.sub(", [Tt][Hh][Ee]", "", n).strip()
                to_add.append(no_the)
                to_add.append("The " + no_the)

        possible_game_names = possible_game_names + to_add

        to_add = []
        for n in possible_game_names:
            if n.lower().find(" - ") != -1:
                no_dash = re.sub(" - ", ": ", n).strip()
                to_add.append(no_dash)

        return possible_game_names + to_add

    def _get_wiki_list_urls(self, system_id):
        '''
            Each system type should correspond to some wikipedia urls which list
            the games for that system in a standard format.

            This function gets those urls from the database.
        '''
        urls = []
        conn = sqlite3.connect(self.config['library']['db_location'])
        try:
            cur = conn.cursor()
            for row in cur.execute("SELECT id, url FROM systemWikiUrls WHERE systemId"):
                urls.append((row[0], row[1]))
        finally:
            conn.close()

        return urls

    def find_file_on_wiki(self, system_id, filename):
        '''
            Take the set of game names and check the relevant list pages on
            wikipedia for a link to the game.

            Return a tuple containing the link to the game and the name of the game
            as found on wiki.
        '''
        possible_game_names = self._find_possible_game_names(filename)

        links = []
        for id, url in self._get_wiki_list_urls(system_id):
            if not id in RomImporter.system_url_map:
                response = urllib.request.urlopen(url)
                RomImporter.system_url_map[id] = BeautifulSoup(response.read())

            parsed_html = RomImporter.system_url_map[id]

            [links.append(a) for a in parsed_html.find_all('a') if a.has_attr('href') and a['href'].startswith('/wiki/')]

        for name in possible_game_names:
            for link in links:
                if link.text.lower() == name.lower():
                    return 'http://en.wikipedia.com' + urllib.parse.quote(link['href']), name

        return None, None

    def import_file(self, url, game_name, game_bytes, system_id, disc):
        '''
            Take a single wikipedia url and import the game with the given
            filename with details from that url.
        '''
        response = urllib.request.urlopen(url)
        parsed_html = BeautifulSoup(response.read())

        image_bytes = None
        i = parsed_html.find('table', class_='hproduct').find('img')
        if i:
            image_bytes = urllib.request.urlopen("http:" + i["src"]).read()

        genre_label = parsed_html.find('a', text='Genre(s)')
        genre = ""
        if genre_label:
            genre_link = genre_label.parent.parent.find_next_sibling('td').find('a')

            if genre_link:
                genre = genre_link.text

        rating = self._find_rating(system_id, game_name)

        conn = sqlite3.connect(self.config['library']['db_location'])
        try:
            existing_game_cur = conn.cursor()
            existing_game_cur.execute("SELECT COUNT(1) FROM games WHERE games.name = ?", (game_name,))
            existing = existing_game_cur.fetchone()[0]

            if system_id and existing == 0:
                cur = conn.cursor()
                cur.execute("INSERT INTO games (name, genre, systemId, image, link, rating) VALUES (?,?,?,?,?,?)",
                            (game_name, genre, system_id, image_bytes, url, rating))

                conn.commit()

                id_cur = conn.cursor()
                id_cur.execute("SELECT games.id FROM games WHERE games.name = ?", (game_name,))
                new_id = id_cur.fetchone()[0]

                new_filename = os.path.join(self.config['library']['library_location'], str(new_id), str(disc))
                os.makedirs(os.path.join(self.config['library']['library_location'], str(new_id)))

                with open(new_filename, "wb") as rom_file:
                    rom_file.write(game_bytes)

                return ""
            else:
                return "Game already exists"
        finally:
            conn.close()

        return "Unexpected error"

    def _find_rating(self, system_id, game_name):
        '''
            Get the rating for a particular game from gamerankings.

            Only uses a single game name at the moment to search.
        '''
        rating = None
        conn = sqlite3.connect(self.config['library']['db_location'])
        try:
            cur = conn.cursor()
            cur.execute("SELECT gameRankingsSystemName FROM system WHERE id=?", (system_id, ))
            row = cur.fetchone()

            if row:
                name = row[0]
                url = "http://www.gamerankings.com/browse.html?site={0}&numrev=3&search={1}".format(name, urllib.parse.quote(game_name))
                response = urllib.request.urlopen(url)
                parsed_html = BeautifulSoup(response.read())

                for link in parsed_html.find('div', class_='body').find_all('a'):
                    if link.text.lower() == game_name.lower():
                        rating_text = link.parent.find_next_sibling('td').find('b').text

                        rating = int(rating_text.replace('%','').replace('.',''))
        except Exception as e:
            # Catch exceptions so that the import doesn't fail when not finding a game ranking
            print(str(e))
        finally:
            conn.close()

        return rating

def create_blank_database(db_file):
    '''
        Creates the blank database at the given location.
    '''
    conn = sqlite3.connect(db_file)
    try:
        cur = conn.cursor()
        for s in db_create.db_sql:
            cur.execute(s)

        conn.commit()
    finally:
        conn.close()

def purge_database(db_file, library_location):
    '''
        Removes any games from the database if their rom file no longer exists.
    '''
    pass

def first_time_setup(config):
    if not os.path.exists("static/game_images"):
        os.makedirs("static/game_images")
    if not os.path.exists(config['library']['db_location']):
        create_blank_database(config['library']['db_location'])
    if not os.path.exists(config['library']['library_location']):
        os.makedirs(config['library']['library_location'])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--configfile', required=False, help='Specify the location of the application config file', default='app.config')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read(args.configfile)

    first_time_setup(config)

    importer = RomImporter(config)

    dir_input = ""
    while not os.path.isdir(dir_input):
        dir_input = input("Enter the directory to import from: ")

    system_type_id = -1
    systems = []
    conn = sqlite3.connect(config['library']['db_location'])
    try:
        cur = conn.cursor()
        for row in cur.execute("SELECT id, displayName FROM system"):
            systems.append((row[0], row[1]))
    finally:
        conn.close()

    print("")
    print("Select from the following system types (note all roms in the directory must be from the same system type)")
    for system in systems:
        print("  {0} - {1}".format(system[0], system[1]))
    print("")

    while system_type_id == -1:
        sys_type_id_input = input("Enter the id: ")
        if sys_type_id_input.isdigit():
            if int(sys_type_id_input) in [id for (id, name) in systems]:
                system_type_id = int(sys_type_id_input)

    importer.import_directory(dir_input, ".*", system_type_id)
