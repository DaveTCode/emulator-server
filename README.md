# Emulator Server #

This application acts as a web interface to all your collected ROMs - regardless of what emulator you choose for each game.

It allows you to start up a game remotely (e.g. from a smartphone) and will find information about a game based on it's filename.

*Note that no emulators or games are included with this software.*

## Installation ##

### Windows ###
The application can be downloaded from [here](https://bitbucket.org/DaveTCode/emulator-server/downloads/emulation-server-0.3.zip)

Once downloaded, extract the application to any directory on your local machine. To run the application simply run webserver.exe and then follow the steps in the configuration section.

### Linux ###
*No instructions at present*

## Configuration ##
Once the application is up and running you can visit the webpage at http://127.0.0.1:5000 from the computer it is running on. The application is listening on 0.0.0.0 so you can also visit it from another machine by going to http://ip:5000

Configuration is done via the settings tab found in the top right corner of the application and has the following sections:
* Library
* * You can configure the location of the applications database here as well as the folder in which it stores the games you import.
* Emulators
* * Here you can configure each different system. The application comes pre configured with some systems although none of them have paths to their respective emulators.

## Development ##
The application is split into two parts - a python webserver running on flask and a angularjs web app using bootstrap to save on css.

The requirements for the python section are in requirements.txt and can be installed with 
```
#!python

pip install -r requirements.txt
```


The webserver is currently completely contained in <root>/webserver.py and mostly acts as a very light front end on top of the database. It serves the static files directly and otherwise provides json on relevant routes.

There is also a file "importer.py" which can either be used as a standalone python application to import entire directories of games or (as it is used in the webserver) to import single games.

The web application is contained in <root>/static and is a pretty classic html5 angularjs application.

### Contribution ###
I'm happy to accept any/all collaboration.