var app = angular.module('gameLibrary', [
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap'
]);

app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread.file = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
                scope.fileread.findWikiLink(changeEvent.target.files[0].name);
            });
        }
    }
}]);

app.controller('GameListController', function ($scope, $http, $location) {
    $scope.pageSize = 25;
    $scope.filter = $location.search()['filter'] || '';
    $scope.pageIndex = parseInt($location.search()['page']) || 1;
    $scope.totalItems = 0;
    $scope.alerts = [];
    $scope.sortOrder = $location.search()['sortOrder'] || 'name';
    $scope.sortDir = $location.search()['sortDir'] || 'asc';

    $scope.$watch('filter', function(newFilter, oldFilter) {
        $location.search('filter', newFilter);
        filterGames(newFilter);
    });

    $scope.$watch('pageIndex', function(newPageIx, oldPageIx) {
        $location.search('page', newPageIx);
        changePage(newPageIx)
    });

    $scope.$watch('sortOrder', function(newOrder, oldOrder) {
        $location.search('sortOrder', newOrder);
        filterGames($scope.filter);
    });

    $scope.$watch('sortDir', function(newDir, oldDir) {
        $location.search('sortDir', newDir);
        filterGames($scope.filter);
    });

    $scope.$on('$routeUpdate', function() {
        $scope.filter = $location.search()['filter'] || '';
        $scope.pageIndex = parseInt($location.search()['page']) || 1;
        $scope.sortOrder = $location.search()['sortOrder'] || 'name';
        $scope.sortDir = $location.search()['sortDir'] || 'asc';
    });

    $scope.refresh = function() {
        $http.get('/games').success(function(data) {
            $scope.games = data.result;

            filterGames();
        });
    };

    $scope.refresh();

    $scope.closeAlert = function(alert) {
        var ix = $scope.alerts.indexOf(alert);

        if (ix !== -1) {
            $scope.alerts.splice(ix, 1);
        }
    };

    $scope.changeSortOrder = function(column) {
        if ($scope.sortOrder === column) {
            if ($scope.sortDir === 'asc') {
                $scope.sortDir = 'desc';
            } else {
                $scope.sortDir = 'asc';
            }
        } else {
            $scope.sortOrder = column;
        }
    };

    $scope.playGame = function(game) {
        $http.post('/games/' + game.id + '/1').success(function(result) { // DAT - Hardcoded the disc id for now
            if (result === "No emulator set") {
                $scope.alerts.push({
                    "text": "No emulator set for " + game.system + " - set it <a href='#/settings'>here</a>",
                    "type": "danger"
                });
            }
        });
    };

    $scope.deleteGame = function(game) {
        if (confirm('This will completely remove the game from the library')) {
            $http.delete('/games/' + game.id).success(function(data) {
                $scope.refresh();
            });
        }
    };

    function changePage(ix) {
        if ($scope.games) {
            $scope.currentPage = $scope.pages[ix - 1];
        }
    };

    function filterGames(filter) {
        if ($scope.games) {
            var filteredGames = [];

            for (var ii = 0; ii < $scope.games.length; ii++) {
                if ($scope.games[ii].name.match(new RegExp(filter, "i"))) {
                    filteredGames.push($scope.games[ii]);
                }
            }

            filteredGames.sort(function(a,b) {
                if ($scope.sortDir === 'asc') {
                    return a[$scope.sortOrder] > b[$scope.sortOrder] ? 1 : a[$scope.sortOrder] < b[$scope.sortOrder] ? -1 : 0;
                } else {
                    return a[$scope.sortOrder] < b[$scope.sortOrder] ? 1 : a[$scope.sortOrder] > b[$scope.sortOrder] ? -1 : 0;
                }
            });

            var pages = [];
            for (var ii = 0; ii < Math.ceil(filteredGames.length / $scope.pageSize); ii++) {
                pages.push(filteredGames.slice(ii * $scope.pageSize, (1 + ii) * $scope.pageSize));
            }

            $scope.pages = pages;
            $scope.totalItems = filteredGames.length;

            if ($scope.pageIndex - 1 >= $scope.pages.length) {
                $scope.pageIndex = 1;
            }

            changePage($scope.pageIndex);
        }
    }
});

app.controller('SettingsController', function($scope, $http, $window) {
    $http.get('/settings').success(function(data) {
        for (var ii = 0; ii < data.emulators.length; ii++) {
            data.emulators[ii].isDeleted = false;
            for (var jj = 0; jj < data.emulators[ii].urls.length; jj++) {
                data.emulators[ii].urls[jj].isDeleted = false;
            }
        }

        $scope.settings = data;
        $scope.original = {};
        angular.copy($scope.settings, $scope.original);
    });

    $scope.saving = false;
    $scope.saveStatus = function() {
        if ($scope.saving) {
            return "Saving..."
        } else {
            return "Saved";
        }
    };

    $scope.isDeleted = function(item) {
        return !item.isDeleted;
    };

    $scope.saveData = function() {
        $scope.saving = true;
        angular.copy($scope.settings, $scope.original);
        $http.post('/settings', $scope.settings).success(function(response) {
            $scope.saving = false;
        });
    };

    $scope.cancel = function() {
        $window.history.back();
    };

    $scope.addNewEmulator = function() {
        $scope.settings.emulators.push({
            'id': -1,
            'displayName': null,
            'path': null,
            'gameRankingsName': null,
            'isDeleted': false,
            'open': true,
            'urls': []
        });
    };

    $scope.deleteEmulator = function(emulator) {
        if (emulator.id !== -1) {
            emulator.isDeleted = true;
        } else {
            var ix = $scope.settings.emulators.indexOf(emulator);

            if (ix !== -1) {
                $scope.settings.emulators.splice(ix, 1);
            }
        }
    };

    $scope.addUrl = function(emulator) {
        emulator.urls.push({
            'id': -1,
            'url': '',
            'isDeleted': false
        });
    };

    $scope.removeUrl = function(emulator, url) {
        if (url.id !== -1) {
            url.isDeleted = true;
        } else {
            var ix = emulator.urls.indexOf(url);

            if (ix !== -1) {
                emulator.urls.splice(ix, 1);
            }
        }
    };
});

app.controller('ImportController', function($scope, $http) {
    $scope.systems = [];
    $scope.working = false;
    $http.get('/systems').success(function(data) {
        $scope.systems = data.result;
    });

    $scope.fileInputs = [{
        'type': null,
        'status': '',
        'file': null,
        'wikiLink': null,
        'name': null,
        'disc': 1,
        'readyToImport': function() {
            return this.file === null || this.wikiLink === null || this.name === null;
        },
        'findWikiLink': function(filename) {
            $scope.working = true;
            var that = this;
            $http.post('/games/find', {'system': this.type, 'filename': filename})
            .success(function(data) {
                $scope.working = false;
                that.wikiLink = data.url;
                that.status = 'Ready to Import',
                that.name = data.name;
            })
            .error(function(err) {
                $scope.working = false;
                that.wikiLink = null;
                that.status = 'Game not found',
                that.name = null
            });
        },
        upload: function() {
            $scope.working = true;
            var that = this;
            $http.post('/games', {
                'url': this.wikiLink,
                'name': this.name,
                'system': this.type,
                'fileString': this.file,
                'disc': this.disc
            }).success(function(data) {
                $scope.working = false;
                that.status = 'Import succeeded';
            }).error(function(err) {
                $scope.working = false;
                that.status = 'Import failed';
            });
        },
        labelClass: function() {
            var cl = "label ";
            if (this.status === 'Ready to Import') {
                cl += "label-info";
            } else if (this.status === 'Game not found') {
                cl += 'label-warning';
            } else if (this.status === 'Import succeeded') {
                cl += 'label-success';
            } else if (this.status === 'Import failed') {
                cl += 'label-danger';
            } else {
                cl += 'label-default';
            }
            return cl;
        }
    }];
});

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/games:query?', {
                templateUrl: 'partials/game_table.tpl.html',
                controller: 'GameListController',
                reloadOnSearch: false
            })
            .when('/settings', {
                templateUrl: 'partials/settings.tpl.html',
                controller: 'SettingsController'
            })
            .when('/import', {
                templateUrl: 'partials/import.tpl.html',
                controller: 'ImportController'
            })
            .otherwise({
                redirectTo: '/games'
            });
    }
]);
