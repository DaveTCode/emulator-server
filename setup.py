from distutils.core import setup
import os
import py2exe

setup(console=['webserver.py'],
      data_files=[('', ['app.config']),
                  ('static', ['static/index.html']),
                  ('static/css', ['static/css/app.css', 'static/css/bootstrap.min.css']),
                  ('static/js', ['static/js/app.js', 'static/js/angular.min.js', 'static/js/angular-route.min.js', 'static/js/angular-sanitize.min.js', 'static/js/ui-bootstrap-tpls-0.11.0.min.js']),
                  ('static/partials', ['static/partials/game_table.tpl.html', 'static/partials/import.tpl.html', 'static/partials/settings.tpl.html']),
                  ('static/fonts', ['static/fonts/glyphicons-halflings-regular.eot', 'static/fonts/glyphicons-halflings-regular.svg', 'static/fonts/glyphicons-halflings-regular.ttf', 'static/fonts/glyphicons-halflings-regular.woff']),
                  ('static/img', ['static/img/favicon.ico', 'static/img/stencil_controller.png'])])
